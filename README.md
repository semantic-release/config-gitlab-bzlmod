# @semantic-release/config-gitlab-bzlmod

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> config to publish a [Bazel](https://bazel.build/) module.

## Getting Started

```sh
npm config --location project set registry https://gitlab.arm.com/api/v4/groups/semantic-release/-/packages/npm/
npm install --save-dev @semantic-release/config-gitlab-bzlmod
```

Add the following to `.releaserc.yaml`:

```yaml
extends:
  - "@semantic-release/config-gitlab-bzlmod"
```

Combine that with other plugins and configuration options:

```yaml
extends:
  - "@semantic-release/config-release-channels"
  - "@semantic-release/config-gitlab-bzlmod"
```

## Configuration

### Authentication

#### GitLab

Create a project access token with `api`/`write_repository` permissions.

Add the token as a `GITLAB_TOKEN` CI protected/masked variable.

### Bazel

The configuration includes semantic-release/bazelisk> that allows the `//release:{verify,prepare,publish,notify}` Bazel targets to run.

### `dist`

Any files put in the `dist` directory will be uploaded as release artifacts.

Combined with the semantic-release/bazelisk> Bazel target hooks, this provides an easy way to add built targets to the release.

Create a `bazelisk run -- //release:prepare` target that creates a `dist` directory filled with release assets.
