import test from 'ava';

test('Can load the ECMAscript module', async t => {
	const {default: {plugins}} = await import('../config.mjs');

	t.is(plugins.length, 9);
});
