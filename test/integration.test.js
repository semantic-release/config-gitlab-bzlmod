// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', t => {
	// eslint-disable-next-line unicorn/prefer-module
	const {plugins} = require('../config.js');

	t.is(plugins.length, 9);
});
